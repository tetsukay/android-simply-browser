package com.example.tetsukay.browser

import android.app.Application
import com.example.tetsukay.browser.core.navigation.Navigator
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            // Android context
            androidContext(this@App)
            // modules
            modules(modules)
        }
    }

    val modules = module {
        factory { Navigator() }
    }
}