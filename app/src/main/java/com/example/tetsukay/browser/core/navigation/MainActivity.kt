package com.example.tetsukay.browser.core.navigation

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.webkit.WebView
import android.widget.Toast
import com.example.tetsukay.browser.R
import com.example.tetsukay.browser.feature.webview.WebViewFragment

import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import java.lang.RuntimeException

class MainActivity : AppCompatActivity() {

    val navigator: Navigator by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("LOG", navigator.toString())

        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.bottom_item_webview -> {
                    setFragment(WebViewFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.bottom_item_bookmark -> {
                    val sp: SharedPreferences = applicationContext.getSharedPreferences("bookmark", Context.MODE_PRIVATE)
                    val url = sp.getString("url", null)
                    if(url == null) {
                        Toast.makeText(applicationContext, "まだBookmarkされていません", Toast.LENGTH_LONG).show()
                        return@setOnNavigationItemSelectedListener true
                    }
                    findViewById<WebView?>(R.id.web_view)?.loadUrl(url)
                    return@setOnNavigationItemSelectedListener true
                }
                else -> throw RuntimeException("Undefined item id: ${it.itemId}")
            }
        }

        go.setOnClickListener {
            findViewById<WebView?>(R.id.web_view)?.loadUrl(url.text.toString())
        }

        add_bookmark.setOnClickListener {
            val url: String = url.text.toString()
            val sp: SharedPreferences = applicationContext.getSharedPreferences("bookmark", Context.MODE_PRIVATE)
            sp.edit().putString("url", url).apply()
        }

    }

    fun setUrlText(u: String) {
        url.setText(u)
    }

    private fun setFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit()
    }

}
