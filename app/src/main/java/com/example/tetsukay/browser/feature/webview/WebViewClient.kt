package com.example.tetsukay.browser.feature.webview

import android.webkit.WebResourceRequest
import android.webkit.WebView


class WebViewClient(
    val onPageFinished: (String) -> Unit
) : android.webkit.WebViewClient() {

    override fun onPageFinished(view: WebView, url: String) {
        onPageFinished(url)
    }
}
