package com.example.tetsukay.browser.feature.webview

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tetsukay.browser.R
import com.example.tetsukay.browser.core.navigation.MainActivity
import kotlinx.android.synthetic.main.webview_fragment.*

class WebViewFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.webview_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        web_view.webViewClient = WebViewClient { url ->
            val activity: MainActivity = requireActivity() as MainActivity
            activity.setUrlText(url)
        }
        web_view.loadUrl("https://www.yahoo.co.jp/")


    }

}